# Reign Full-Stack Developer Test
This solution requires **Git and Docker** (node.js, npm and Postman optionals). The links to download these tools are bellow:
- https://git-scm.com/downloads
- https://www.docker.com/products/docker-desktop
- https://nodejs.org/
- https://www.postman.com/downloads/

In order to run this solution, please follow the next steps in command line.

1. Clone this repo.

```ssh
git clone git@gitlab.com:carranza.l/reigntest.git
```

2. Run the Docker-Compose file.
```ssh
cd ./reigntest/
docker-compose up --build -V
```

3. Drink a coffee waiting the apps to finish their configurations ☕.

4. Populate the database for the first time. I use Postman since it has a nice interface but curl or Invoke-RestMehod in cmd is ok too. In fact, you can even call it with your browser (code in this way to ease this step 😅). The **GET** api is the following. 

- http://localhost:3003/articles/pullData


And that's it! You will have access to the app via http://localhost:3000/
The app is programmed to update the data each hour (12:00, 13:00, 14:00, ...).

I thank you for this challenge which has been very interesting to develop! 😄
