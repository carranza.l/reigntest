import { Module, HttpModule  } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Article } from './entity/article.entity';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Article]),HttpModule],
  providers: [ArticleService],
  controllers: [ArticleController]
})
export class ArticleModule {}