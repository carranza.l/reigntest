import {
    Body,
    Controller,
    Delete,
    Get,
    Post,
  } from '@nestjs/common';
  import { Article } from './entity/article.entity';
  import { ArticleService } from './article.service';
  import { DeleteResult } from 'typeorm';
  
  @Controller('articles')
  export class ArticleController {
  
    constructor(private readonly articleService: ArticleService) {}
  
    @Post()
    public async create(@Body() articleDto: any): Promise<Article> {
      return await this.articleService.create(articleDto);
    }

    @Get('pullData')
    public async pullData() {
      await this.articleService.pullData();
      return {};
    }

    @Get()
    public async list(): Promise<Article[]> {
      return await this.articleService.list();
    }

    @Delete()
    public async remove(@Body() articleDto: any): Promise<DeleteResult> {
      return await this.articleService.remove(articleDto);
    }
  
  }