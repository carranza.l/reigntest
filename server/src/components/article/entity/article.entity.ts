import {
  Column,
  Entity, ObjectIdColumn,
} from 'typeorm';

@Entity({name: 'Article'})
export class Article {
  @ObjectIdColumn()
  id: number;

  @Column({
    type: 'string'
  })
  story_title: string;

  @Column({
    type: 'string'
  })
  title: string;

  @Column({
    type: 'string'
  })
  story_url: string;

  @Column({
    type: 'string'
  })
  url: string;

  @Column({
    type: 'string'
  })
  author: string;

  @Column({
    type: 'string'
  })
  created_at: string;

}