import { Injectable, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Article } from './entity/article.entity';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article)
    private readonly articleRepository: Repository<Article>,
    private httpService: HttpService,
  ) {}

  public async create(articleDto: any): Promise<Article> {
    const article = new Article();
    article.story_title = articleDto.story_title;
    article.title = articleDto.title;
    article.story_url = articleDto.story_url;
    article.url = articleDto.url;
    article.author = articleDto.author;
    article.created_at = articleDto.created_at;
    return await this.articleRepository.save(article);
  }

  public async list(): Promise<Article[]> {
    return await this.articleRepository.find();
  }

  public async remove(articleDto: any): Promise<DeleteResult> {
    return await this.articleRepository.delete(articleDto.id);
  }

  @Cron('* 0 * * * *')
  public async pullData() {
    const oldArticleList = await this.articleRepository.find();
    await this.articleRepository.remove(oldArticleList);
    const newArticleList = (await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise()).data.hits;
    var i;
    for (i = 0; i < newArticleList.length; i++) {
        this.create(newArticleList[i]);
    }
  }
}