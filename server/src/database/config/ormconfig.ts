export function ormConfig(): any {
    return {
      type: 'mongodb',
      host: 'db',
      port: 27017,
      username: 'root',
      password: 'root',
      synchronize: true,
      logging: true,
      autoLoadEntities: true,
      useUnifiedTopology: true,
      useNewUrlParser: true,
      connectTimeout: 15000,
      acquireTimeout: 15000,
      extra: {
        connectionLimit: 20,
      },
      entities: [
        'dist/**/entity/*.entity.js',
      ],
      migrations: [
        'dist/database/migrations/*.js',
      ],
      subscribers: [
        'dist/observers/subscribers/*.subscriber.js',
      ],
      cli: {
        entitiesDir: 'src/components/**/entity',
        migrationsDir: 'src/database/migrations',
        subscribersDir: 'src/observers/subscribers',
      },
    };
  }