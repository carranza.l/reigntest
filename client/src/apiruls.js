import axios from "axios";

const api = axios.create({
    baseURL: "http://localhost:3003/articles/",
    timeout: 5000
});

export const getArticles = async() => {
    const response = await api({
        method: "get",
        url: ""
    });
    if(response.status >= 200 && response.status < 300) return response.data;
    else return [];
}

export const deleteArticle = async(item) => {
    const response = await api({
        method: "delete",
        url: "",
        data: item
    });
    if(response.status >= 200 && response.status < 300) return response.data;
    else return {};
}