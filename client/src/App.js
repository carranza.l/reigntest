import './App.css';
import trashIcon from './icons/trash-alt-solid.svg';
import { getArticles, deleteArticle } from './apiruls';
import {useState, useEffect} from 'react';
 
const List = ({ list, setArticleList }) => (
  <div>
    {(list || []).map(item => (
      <ListItem key={item.id} item={item} setArticleList={setArticleList} />
    ))}
  </div>
);

function ListItem  ( {item, setArticleList} )  {
  const [isHovered, setisHovered] = useState(false);

  function handleClick(item) {
    window.location = item.story_url?item.story_url:item.url;
  }

  async function handleErase(item) {
    await deleteArticle(item);
    var articles = await getArticles();
    articles = articles.filter(function(article) {
      return article.story_title !== "" || article.title !== "";
    }).sort(function(a,b) {
      return b.created_at - a.created_at
    });
    setArticleList(articles.filter(function(article) {
      return article.story_title !== "" || article.title !== "";
    }).sort(function(a,b) {
      return b.created_at - a.created_at
  }));
  }

  function setTrash() {
    setisHovered(true);
  };
  function coverTrash() {
    setisHovered(false);
  };
  function setDate(dateString){
    var today = new Date();
    var ddToday = String(today.getDate()).padStart(2, '0');
    var mmToday = String(today.getMonth() + 1).padStart(2, '0');
    var yyToday = today.getFullYear().toString();
    const yearNumber = dateString.substring(0,4);
    const monthNumber = dateString.substring(5,7);
    const dayNumber = dateString.substring(8,10);
    if(yearNumber === yyToday && monthNumber === mmToday && dayNumber === ddToday){
      if(parseInt(dateString.substring(11,13))===0){
        return "12:"+dateString.substring(14,16) + " am";
      }
      else if(parseInt(dateString.substring(11,13))<12){
        return dateString.substring(11,13)+":"+dateString.substring(14,16) + " am";
      }
      else if(parseInt(dateString.substring(11,13))===12){
        return dateString.substring(11,13)+":"+dateString.substring(14,16) + " pm";
      }
      else {
        return (parseInt(dateString.substring(11,13))-12)+":"+dateString.substring(14,16) + " pm";
      }
    }
    else{
      var yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);
      var yyYesterday = yesterday.getFullYear().toString();
      var ddYesterday = String(yesterday.getDate()).padStart(2, '0');
      var mmYesterday = String(yesterday.getMonth() + 1).padStart(2, '0');
      if(yearNumber === yyYesterday && monthNumber === mmYesterday && dayNumber === ddYesterday){
        return "Yesterday"
      }
      else{
        const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dec'];
        return months[monthNumber-1]+" "+dateString.substring(8,10);
      }
    }
  }

  return (
  <div class="rowBackgroundColor">
    <div class="row" onMouseOver={setTrash} onMouseLeave={coverTrash}>
      <div class="columnInfo" onClick={() => handleClick(item)}>
        {item.story_title?item.story_title:item.title} <a class="author">- {item.author} - </a>
      </div>
      <div class="columnDate" onClick={() => handleClick(item)}>
        {setDate(item.created_at)}
      </div>
      <div class="columnErase" onClick={() => handleErase(item)}>
        {isHovered?<img src={trashIcon} className="trash-icon" alt="trash-icon" />:""}
      </div>
    </div>
    <hr class="hrLine" />
  </div>
  )

};

function App() {
  const [articleList, setArticleList] =useState([]);

  useEffect(()=>{
    onPageRendered();
  },[]);

  const onPageRendered = async () => {
    listArticles();
  };
  const listArticles= async()=>{
    var articles = await getArticles();
    articles = articles.filter(
      function(article) {
        return article.story_title !== "" || article.title !== "";
      }
    ).sort(
      function(a,b) {
        return b.created_at - a.created_at;
      }
    ); 
    setArticleList(articles);
  };

  return (
    <div>
      <header className="App-header">
        <h1 className="Header">
          HN Feed!
        </h1>
        <h2 className="Subheader">
          {"We <3 hacker news!"}
        </h2>
      </header>
      <List list={articleList} setArticleList={setArticleList}/>
    </div>
  );
}

export default App;
